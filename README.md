# KNIME ML Pipeline WIth Cloud Storage

Please refer to PNG Files. Following is a short guide.

1. Please import "Credit_Card_Rating.knwf" the workflow contains 
  a. Blue Highlighted Containers  The three descriptive containers
    i. Take input from the "csv" and "excel" sources. (Please change the location of the files.)
    ii. Combine the two sources and convert string to integer classes.
    iii. Use sampling and then train the model. Save options are both PMML and Model files.
  b. Orange Highlighted Containers  The container contains the performance of our model and thus we use cross validation to reach a score.
    i. Score and feature performance metrics are stored to BIRT reports via Reporting Nodes. (Attached are birt_report.html and birt_report.pdf)
  c. Green Highlighted Containers  The three containers provide a solution to using cloud hosted storage
    i. Authenticate and use Google Cloud Storage (Attached is my *.p12 file with the privilege to read and write, please keep it private.)
    ii. Use pre-processing modules as used earlier
    iii. Test the model we have already trained above using files from Storage.
2. Please import "Credit_Score_Custom_Check.knfw" the workflow contains 
  a. Two components are present "Input Value Display" which outputs data to be used as input "Predict" 
  b. Component "Input Value Display" can be right clicked for "Interactive Display" and shows input form for user to enter custom data. Widgets are used.
  c. Component "Predict" can be right clicked for "Interactive Display" and shows the prediction.
    i. Uses the saved input model file for prediction. (Attached is "output_model.model") 
    ii. View is used to send back the data.
